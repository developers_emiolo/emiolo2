<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'emiolo');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@U;{eJ3BID?g]^&#YivN_Eu_Vz$;XM|G~e)HHgarEiP)l+O@:j4I9V+&AuZ3W-$S');
define('SECURE_AUTH_KEY',  '0v1FUVNdEt8K@|:DytkafSL20nKu|6)bak]AJv]9]DjUn$DGH%U3/pTY0=}%BmEu');
define('LOGGED_IN_KEY',    ' yu6TKvCS*K:V>-us8~XGM2)M[bF#VcA%v$jV;[{gFS*%|BEv$68=+tG@`goEu::');
define('NONCE_KEY',        '4M6o{u!tg7XW-oeM|4r>G#xwiOi+#Q~fyK7OcCGi_hCP tG.a OeU-4}E.0Kz>W)');
define('AUTH_SALT',        'gf_KlBs[i-x2;rC4>!DBJOWiis>HyNM*?xGm+[=o4le)%:Ew[AZuQ*T;Aj`9@jpK');
define('SECURE_AUTH_SALT', '*i):N:A3vhU=Zs@tR_$Vt6V%MslccIz$E63e6}>dx#wGjtrIG=W{E6fo@z?uk_jg');
define('LOGGED_IN_SALT',   'n$ontgmf;@,:C0^0G-<gjwtR.08@::]~*X$!I[Hi R=k,roHDWjQN?iawhAV&1k!');
define('NONCE_SALT',       'pV}L-cz#hgd?K=oa&XF4,J[,AvvsgXHYi=;NU|e6@x*|Zs@m]i,1LxX2<hpUJN;Q');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
